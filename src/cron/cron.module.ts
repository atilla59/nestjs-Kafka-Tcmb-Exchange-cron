import { BullModule, InjectQueue } from '@nestjs/bull';
import { Logger, Module, OnModuleInit } from '@nestjs/common';
import { JobId, Queue } from 'bull';
import { CronProcessor } from './cron.processor';
import { CronService } from './cron.service';
import { HttpModule } from '@nestjs/axios';
import { ProducerService } from '../kafka/producer.service';
import { KafkaModule } from '../kafka/kafka.module';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'tcmb-exchange',
    }),
    HttpModule,
    KafkaModule,
  ],
  providers: [CronProcessor, CronService],
  exports: [CronService],
})
export class CronModule implements OnModuleInit {
  constructor(
    @InjectQueue('tcmb-exchange') private readonly tcmbExchage: Queue,
  ) {}
  async onModuleInit(): Promise<any> {
    Logger.log(' CronModule  - onModuleInit');

    const isExchangeCronExist = await this.tcmbExchage.getRepeatableCount();

    //
    const repeatble = await this.tcmbExchage.getRepeatableJobs();

    repeatble.map((r) =>
      // this.tcmbExchage.removeRepeatableByKey('fetch-data:::3000'),
      Logger.log(JSON.stringify(r)),
    );

    if (isExchangeCronExist === 0) {
      Logger.log('Exchange Cron not found adding.....');

      await this.tcmbExchage.add(
        'fetch-data',
        { url: 'https://www.tcmb.gov.tr/kurlar/today.xml' },
        { repeat: { every: 1000 * 60 } },
      );
    }
  }
}
