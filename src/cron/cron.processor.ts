import { HttpService } from '@nestjs/axios';
import { InjectQueue, Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job, Queue } from 'bull';
import { xml2json } from 'xml-js';
import { ProducerService } from '../kafka/producer.service';

@Processor('tcmb-exchange')
export class CronProcessor {
  private readonly logger = new Logger(CronProcessor.name);

  constructor(
    private httpService: HttpService,
    private producerService: ProducerService,
  ) {}

  @Process('fetch-data')
  async handleTranscode(job: Job) {
    this.logger.debug('fetch-data from tcmb');
    const data = this.httpService
      .get('https://www.tcmb.gov.tr/kurlar/today.xml')
      .toPromise()
      .then((res) => (res = res.data));

    const jsonResult = JSON.parse(
      xml2json(await data, { compact: true, spaces: 4 }),
    );
    //[  {Name: "USDTRY" , value : "11111" } ,  {Name: "EUTRY" , value : "2222" }]

    const resArr = [];
    jsonResult.Tarih_Date.Currency.map((currency) => {
      resArr.push({
        Name: currency._attributes.Kod,
        value: currency.ForexBuying._text,
      });
    });

    await this.producerService.produce({
      topic: 'tcmb-exchange',
      messages: resArr,
    });
    this.logger.debug('fetch-data completed');
  }
}
