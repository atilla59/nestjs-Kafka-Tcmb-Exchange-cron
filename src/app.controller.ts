import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { HttpService } from '@nestjs/axios';
import convert, { xml2json } from 'xml-js';
import { ProducerService } from './kafka/producer.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private httpService: HttpService,
    private producerService: ProducerService,
  ) {}

  @Get()
  async getHello() {}
}
