import { Module } from '@nestjs/common';
import { ConsumerService } from './consumer.service';
import { ProducerService } from './producer.service';
import { CronService } from '../cron/cron.service';

@Module({
  providers: [ProducerService, ConsumerService, CronService],
  exports: [ProducerService, ConsumerService],
  imports: [],
})
export class KafkaModule {}
